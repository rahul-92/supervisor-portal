'use strict';

angular.module('myApp', [
  'myApp.supervisor.admin',
  'myApp.worker.service',
  'myApp.assign.task',
  'myApp.toastr.notification.service'
])
.controller('sharedCtrl', function(workerService, myToastr, $interval){
  this.id = -1, this.progress = false, this.showTask = false, this.noDuration = false;
  this.selectedClass = false, this.interval = null, this.count = 0;
  this.tempWorker = workerService.tempWorker;
  workerService.workers().then((result) => this.workers = result.data.workers);
  
  this.toggleShowTask = (id, hovDiv) => {
    if(this.workers[id-1].taskDuration > 0){
      myToastr.notification('i', 5);
      return;
    }
    hovDiv = true; this.id = id;
    if(!this.showTask) {
      this.showTask = !this.showTask;
    }
    this.tempWorker.name = this.workers[id-1].name;
    this.tempWorker.taskAssigned = this.workers[id-1].taskAssigned;
    this.tempWorker.taskDuration = this.workers[id-1].taskDuration;
    
  }

  this.savetask = () => {
    this.noDuration = false;
    this.showTask = !this.showTask;
    this.workers[this.id-1].taskAssigned = this.tempWorker.taskAssigned;
    this.workers[this.id-1].taskDuration = this.tempWorker.taskDuration;
    myToastr.notification('s', 1);     
  }

  this.startTimer = () => {
    if(this.noDuration) {
      myToastr.notification('w', 6);
      return;
    }
    myToastr.notification('s', 2);
    this.progress = !this.progress;
    this.interval =  $interval(()=> {
      this.workers.forEach((worker) => {
        if(worker.taskDuration > 0) {
          this.count = 0;
          worker.taskDuration == 1 ? worker.taskAssigned = worker.taskDuration = "" : worker.taskDuration--;
        } else {
          this.count++;
        }
        if(this.count == this.workers.length) {
          this.noDuration = true;
          this.stopTimer(false);
        }
      });
    }, 1000);
    
  }

  this.stopTimer = (flag) => {
    flag ? myToastr.notification('e', 3): myToastr.notification('w', 4);
    this.progress = !this.progress;
    $interval.cancel(this.interval)
  }

});
