'use strict';

angular.module('myApp.worker.service', [])

.service('workerService', ['$http', function($http) {

        this.workers = function() { 
            return $http.get('modules/data/workers.json') 
        }
       
        this.tempWorker = {
            name: "",
            taskAssigned: "Hello",
            taskDuration: 0
        };
    }
]);