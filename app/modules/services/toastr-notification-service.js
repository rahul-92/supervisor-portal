'use strict';
const NOTIFICATION = {
    SAVE_TASK: {
        TITLE: "Awesome!!",
        VALUE: "Worker has assigned with updated task and duration."
    },
    START_TIMER: {
        TITLE: "Work Started!",
        VALUE: "Enjoy learning by doing..."
    },
    STOP_TIMER: {
        MANUAL: {
            TITLE: "Congrats!!!",
            VALUE: "All workers are free!"
        },
        AUTO: {
            TITLE: "Work Stopped!",
            VALUE: "Enjoy your day..."
        }
    },
    UPDATE_ALERT: {
        TITLE: "Oops! worker is not free.",
        VALUE: "Please wait, untill he finished with!"
    },
    START_ALERT: {
        TITLE: "Oops!",
        VALUE: "Workers are already free...Please assign task."
    }
}

const MSG_FLAG = {
    SAVE_TASK: 1,
    START_TIMER: 2,
    STOP_TIMER: {
        AUTO: 3,
        MANUAL: 4
    },
    UPDATE_ALERT: 5,
    START_ALERT: 6
}
angular.module('myApp.toastr.notification.service', [])

.service('myToastr', [function() {
        this.title = "", this.value = "";
        this.notification = (typeFlag, msgFlag) => { 
            switch(msgFlag) {
                case MSG_FLAG.SAVE_TASK:
                    this.title = NOTIFICATION.SAVE_TASK.TITLE;
                    this.value = NOTIFICATION.SAVE_TASK.VALUE
                    break;

                case MSG_FLAG.START_TIMER:
                    this.title = NOTIFICATION.START_TIMER.TITLE;
                    this.value = NOTIFICATION.START_TIMER.VALUE
                    break;

                case MSG_FLAG.STOP_TIMER.AUTO:
                    this.title = NOTIFICATION.STOP_TIMER.AUTO.TITLE;
                    this.value = NOTIFICATION.STOP_TIMER.AUTO.VALUE
                    break;
                
                case MSG_FLAG.STOP_TIMER.MANUAL:
                    this.title = NOTIFICATION.STOP_TIMER.MANUAL.TITLE;
                    this.value = NOTIFICATION.STOP_TIMER.MANUAL.VALUE
                    break;

                case MSG_FLAG.UPDATE_ALERT:
                    this.title = NOTIFICATION.UPDATE_ALERT.TITLE;
                    this.value = NOTIFICATION.UPDATE_ALERT.VALUE
                    break;
                    
                case MSG_FLAG.START_ALERT:
                    this.title = NOTIFICATION.START_ALERT.TITLE;
                    this.value = NOTIFICATION.START_ALERT.VALUE
                    break;
            }

            toastr.clear();
            
            switch(typeFlag) {
                case 'i':
                    toastr.info(this.value, this.title);
                    break;
                case 's':
                    toastr.success(this.value, this.title);
                    break;
                case 'w':
                    toastr.warning(this.value, this.title);
                    break;
                case 'e':
                    toastr.error(this.value, this.title);
                    break;
            }

        }
        
    }
]);