'use strict';

angular.module('myApp.assign.task', [])

.directive('assignTask', function() {
  return  {
    templateUrl: 'modules/templates/assign-task.html',
    scope: {
      tempWorker: '=',
      savetask: '&'      
    }
  };
});