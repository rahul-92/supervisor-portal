'use strict';

angular.module('myApp.supervisor.admin', [])

.directive('supervisorAdmin', function() {
  return  {
    templateUrl: 'modules/templates/supervisor-admin-page.html',
    replace: true
  };
});
